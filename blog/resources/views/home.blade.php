<!DOCTYPE html>
<!-- Author ="I Made Tangkas Wahyu Kencana Yuda" -->
<html lang="en">

<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Sanberbook Hari 12 – Membuat Web Statis dengan Laravel</title>
    </head>

    <body>
        <h1>SanberBook</h1>
        <h2>Social Media Developer Santai Berkualitas</h2>

        <p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>

        <h3>Benefit Join di SanberBook</h3>
        <ul>
            <li>Mendapatkan motivasi dari sesama developer</li>
            <li>Sharing knowledge dari para mastah Sanber</li>
            <li>Dibuat oleh calon web developer terbaik</li>
        </ul>

        <h3>Cara Bergabung ke SanberBook</h3>
        <ol>
            <li>Mengunjungi Website ini</li>
            <li>Mendaftar di <a href="/register">Form Sign Up</a></li>
            <li>Selesai!</li>
        </ol>
        <p>&copy; SanberBook</p>
    </body>
</html>
